// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElement.h"
#include "cmath"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	MoveSpeed = pow(5,-1);
	LastMoveDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MoveSpeed);
	AddSnakeElement(2);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnake::AddSnakeElement(int ElementsNum)
{
	for (int i = 0;i < ElementsNum;++i)
	{
		FVector NewLocation(-(SnakeElements.Num() * ElementSize), 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElement* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnake::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MoveSpeed = ElementSize;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MoveSpeed;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MoveSpeed;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MoveSpeed;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MoveSpeed;
		break;
	}

	SnakeElements[0]->ToggleCollision();
	SnakeElements[0]->SetActorHiddenInGame(false);

	for (int i = SnakeElements.Num() - 1; i > 0;i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->SetActorHiddenInGame(false);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnake::SnakeElementOverlap(ASnakeElement* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractacleInterface = Cast<IInteractable>(Other);
		if (InteractacleInterface)
		{
			InteractacleInterface->Interact(this, bIsFirst);
		}
	}
}

