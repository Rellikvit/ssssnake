// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Snake.h"
#include "Food.h"
#include "Components/InputComponent.h"

FTimerHandle FoodTimerHandle;

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90,0,0));
	CreateSnakeActor();
	GetWorld()->GetTimerManager().SetTimer(FoodTimerHandle, this, &APlayerPawnBase::CreateFood, 3, true);
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
	PlayerInputComponent->BindAction("Slowness", IE_Pressed, this, &APlayerPawnBase::StartSlowDown);
	PlayerInputComponent->BindAction("Speed", IE_Pressed, this, &APlayerPawnBase::StartSpeedUp);
	PlayerInputComponent->BindAction("Slowness", IE_Released, this, &APlayerPawnBase::EndSlowDown);
	PlayerInputComponent->BindAction("Speed", IE_Released, this, &APlayerPawnBase::EndSpeedUp);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnake>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::CreateFood()
{
	int32 maxX = 500, minX = -500;
	int32 rangeX = maxX - minX + 1;
	int32 numX = rand() % rangeX + minX;
	int32 maxY = 500, minY = -500;
	int32 rangeY = maxY - minY + 1;
	int32 numY = rand() % rangeY + minY;
	FVector NewLocation((int32)(numX%10)*50,(int32)(numY % 10)*50, 20);
	FTransform NewTransform(NewLocation);
	FoodMas.Add(FoodActor);
	FoodActor = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
	if (FoodActor)
	{
		FString LocationX = FString::SanitizeFloat(FoodActor->GetActorLocation().X);
		FString LocationY = FString::SanitizeFloat(FoodActor->GetActorLocation().Y);
		FString LocationZ = FString::SanitizeFloat(FoodActor->GetActorLocation().Z);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Location: ") + LocationX + LocationY + LocationZ);
	}
}



void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
		}
	}
}

void APlayerPawnBase::StartSlowDown()
{
	if (IsValid(SnakeActor))
	{
		SnakeActor->SetActorTickInterval(SnakeActor->GetActorTickInterval() + 0.3);
	}
}

void APlayerPawnBase::StartSpeedUp()
{
	if (IsValid(SnakeActor))
	{
		SnakeActor->SetActorTickInterval(SnakeActor->GetActorTickInterval() - 0.1);
	}
}

void APlayerPawnBase::EndSlowDown()
{
	if (IsValid(SnakeActor))
	{
		SnakeActor->SetActorTickInterval(SnakeActor->GetActorTickInterval() - 0.3);
	}
}

void APlayerPawnBase::EndSpeedUp()
{
	if (IsValid(SnakeActor))
	{
		SnakeActor->SetActorTickInterval(SnakeActor->GetActorTickInterval() + 0.1);
	}
}


